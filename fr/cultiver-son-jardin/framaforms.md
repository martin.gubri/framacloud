# Installation de Framaforms

Le tutoriel d'installation est en cours de rédaction. Le dépôt de
Framaform contient des [instructions auxquelles vous référer](https://framagit.org/framasoft/framaforms/-/blob/master/README.md#installation) 
en attendant.

Toutes nos excuses pour le délai - nous faisons vraiment de notre mieux !

L'équipe Framasoft.